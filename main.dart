// ignore_for_file: deprecated_member_use

import 'dart:ui';

import 'package:flutter/material.dart';

import 'screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const LogInScreen(),
    );
  }
}

class LogInScreen extends StatefulWidget {
  const LogInScreen({Key? key}) : super(key: key);

  @override
  State<LogInScreen> createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  late String _name;
  late String _Password;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Name Is Required";
        }
      },
      onSaved: (String? value) {
        _name = value!;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      decoration: const InputDecoration(labelText: 'Password'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Password Is Required";
        }
      },
      onSaved: (String? value) {
        _Password = value!;
      },
    );
    ;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('LogIn/Register'),
      ),
      body: Container(
        margin: const EdgeInsets.all(70),
        child: Form(
          child: Column(
            children: [
              _buildName(),
              const SizedBox(
                height: 50,
              ),
              _buildPassword(),
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    child: const Text(
                      "SignIn",
                      style: TextStyle(color: Colors.blue, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (BuildContext) {
                        return DashBoard();
                      }));
                    },
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  RaisedButton(
                    child: const Text(
                      'Register',
                      style: TextStyle(color: Colors.blue, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (BuildContext) {
                        return RegisterScreen();
                      }));
                    },
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

/*Building the user registration screen
The user will register by filling in their full name, email, and creating 
password*/
class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late String _nameForRegistration;
  late String _emailForRegistration;
  late String _PasswordForRegristration;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

//Function 1-3 are widget functions for user name, email, and creating functions
  Widget _buildName() {
    //Fuction 1
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Name Is Required";
        }
      },
      onSaved: (String? value) {
        _nameForRegistration = value!;
      },
    );
  }

  Widget _buildEmail() {
    //Function 2
    return TextFormField(
      decoration: InputDecoration(labelText: 'Email'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Email Is Required";
        }
      },
      onSaved: (String? value) {
        _emailForRegistration = value!;
      },
    );
  }

  Widget _buildPassword() {
    //Function 3
    return TextFormField(
      decoration: InputDecoration(labelText: 'Password'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Password Is Required";
        }
      },
      onSaved: (String? value) {
        _PasswordForRegristration = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: Container(
        margin: EdgeInsets.all(50),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              _buildName(),
              const SizedBox(
                height: 50,
              ),
              _buildEmail(),
              const SizedBox(
                height: 50,
              ),
              _buildPassword(),
              const SizedBox(
                height: 100,
              ),
              RaisedButton(
                child: const Text(
                  'Submit',
                  style: TextStyle(color: Colors.blue, fontSize: 16),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) {
                    return LogInScreen();
                  }));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}






















/*class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOption = <Widget>[
    Text(
      "Index 0: Home",
      style: optionStyle,
    ),
    Text(
      "Index 1: Dashboard",
      style: optionStyle,
    ),
    Text(
      "Index 2: Email",
      style: optionStyle,
    ),
    Text(
      "Index 3: Person",
      style: optionStyle,
    )
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My First Project"),
      ),
      body: Center(
        child: _widgetOption.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Colors.indigo,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.dashboard),
            label: 'Dashboard',
            backgroundColor: Colors.indigo,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.email),
            label: 'Email',
            backgroundColor: Colors.indigo,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Person',
            backgroundColor: Colors.indigo,
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}*/
