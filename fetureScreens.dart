// ignore_for_file: deprecated_member_use

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_application_2/screen.dart';

//Creating two feature screens

class FeatureScreen1 extends StatefulWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  State<FeatureScreen1> createState() => _FeatureScreen1State();
}

class _FeatureScreen1State extends State<FeatureScreen1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Feature 1'),
      ),
      body: Container(
        // ignore: sort_child_properties_last
        child: Column(
          children: <Widget>[
            const Text(
              'Feature 1: App Academy Rocks',
              style: TextStyle(
                color: Colors.indigoAccent,
                fontSize: 28,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            RaisedButton(
              child: const Text('Feature 2',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 30,
                  )),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext) {
                  return FeatureScreen2();
                }));
              },
            ),
            const SizedBox(
              height: 20,
            ),
            RaisedButton(
              child: const Text('Dashboard',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 30,
                  )),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext) {
                  return DashBoard();
                }));
              },
            )
          ],
        ),
        color: Color.fromARGB(190, 2, 13, 77),
        alignment: Alignment.center,
        padding: const EdgeInsets.all(180),
        constraints: const BoxConstraints.tightForFinite(
          width: 800,
        ),
      ),
    );
  }
}

class FeatureScreen2 extends StatefulWidget {
  const FeatureScreen2({Key? key}) : super(key: key);

  @override
  State<FeatureScreen2> createState() => _FeatureScreen2State();
}

class _FeatureScreen2State extends State<FeatureScreen2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Feature 2'),
      ),
      body: Container(
        // ignore: sort_child_properties_last
        child: Column(
          children: <Widget>[
            const Text(
              'Feature 2: You Rock',
              style: TextStyle(
                color: Colors.indigoAccent,
                fontSize: 28,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            RaisedButton(
              child: const Text('Feature 1',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 30,
                  )),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext) {
                  return FeatureScreen1();
                }));
              },
            ),
            const SizedBox(
              height: 20,
            ),
            RaisedButton(
              child: const Text('Dashboard',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 30,
                  )),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext) {
                  return DashBoard();
                }));
              },
            )
          ],
        ),
        color: Color.fromARGB(199, 3, 39, 68),
        alignment: Alignment.center,
        padding: const EdgeInsets.all(180),
        constraints: const BoxConstraints.tightForFinite(
          width: 800,
        ),
      ),
    );
  }
}
